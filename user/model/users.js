"use-strict"
let mongoose = require('mongoose');
let schema = mongoose.Schema;

var userSchema = new schema({
    username:{
        type:String,
        required: "Username is required!"
    },
    password:{
        type:String,
        required: "Password is required!"
    },
    created_date:{
        type:Date,
        default: Date.now
    },

    email:{
        type:String,
        required:"Email is required"
    },
    first_name:{
        type:String,
        required:"First name is required"
    },
    last_name:{
        type:String,
        default:null
    },
    gender:{
        type:Number,
        required:"Gender is required"
    },
    last_login:{
        type:Date,
        default:Date.now
    },
    last_modified:{
        type:Date,
        default:Date.now
    },
    avatar:{
        type:String,
        default:null
    },
    enabled:{
        type:Boolean,
        default:true
    }
});
userSchema.index({username:1,email:1},{unique:true});

module.exports = mongoose.model("Users",userSchema);