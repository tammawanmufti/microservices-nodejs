"use-strict";

const users = require("../model/users");
const response = require("../../_libs/response");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.getAllUsers = function (req, res) {
    users.find()
        .then((data) => {
            response.success(res, "Get all users data success!", data);
        })
        .catch(err => response.badRequest(res, err.message));
}

exports.createUser = function (req, res) {
    req.body['password'] = bcrypt.hashSync(req.body['password'], parseInt(10));
    users.create(req.body)
        .then((data) => response.created(res, "User data created!", data))
        .catch(err => response.badRequest(res, err.message));
}

exports.getUserById = function(req,res){
    users.findOne({ _id:req.params.id})
    .then((data) => !data
        ?response.notFound(res , "User not found")
        :response.success(res , "Get user by id success"),data)
        .catch(err => response.badRequest(res, err.message));
}

exports.updateUser = function(req,res){
    users.findOneAndUpdate({ _id:req.params.id},req.body)
    .then((data) => !data
        ?response.notFound(res,"User not found")
        :response.success(res,"Update user data success"),data)
        .catch(err => response.badRequest(res, err.message));
}

exports.deleteUser = function(req,res){
    users.findOneAndDelete({ _id:req.params.id})
    .then((data) => !data
        ?response.notFound(res , "User not found")
        :response.success(res , "Get user by id success"),data)
        .catch(err => response.badRequest(res, err.message));
}

exports.findByCredentials = function(req, res) {
    let findBy = {
        $or: [
            { email: req.body.email },
            { username: req.body.username }
        ]
    };

    // Find if user exists
    if (!req.body.username && !req.body.email) {
        response.badRequest(res, "Username or email is required!")    
    }

    users.findOne(findBy)
    .then(result => {
        if(!result){
            response.badRequest(res, "Wrong user or password!");
        }
        return result;
    })
    .then(user => {
        // Check password and pass user data
        if (!req.body.password) {
            response.badRequest(res, "Wrong user or password!");
        }
        
        if (!bcrypt.compareSync(req.body.password, user.password)) {
            response.badRequest(res, "Wrong user or password!");
            return false;
        }
        console.log(process.env.JWT_KEY)

        let token = jwt.sign(
            {
                data: user
            },
            process.env.JWT_KEY,
            {
                expiresIn: process.env.JWT_EXPIRED
            }
        )

        let responseData = {
            token: token
        };
        response.success(res, "User logged in!", responseData);
        return true;
    })
    .then(result => {
        // Update last login for logged in user
        if(!result){
            response.notFound(res, "Something went wrong!");
        }
        users.updateOne(findBy, { last_login: new Date() })
        .then((data) => {
            if (!data) {
                console.log("Update last login failed for user: " + (req.body.username || req.body.email));
            }
            console.log("Update last login success for user: " + (req.body.username || req.body.email));
        })
        .catch(err => response.badRequest(res, err));
    })
    .catch(err => response.badRequest(res,err));
}