require('dotenv').config();
let express = require("express");
let response = require("../_libs/response");
let bodyParser = require("body-parser");
let mongoose = require("mongoose");
let cors = require("cors");

//routes
let usersRoute = require("./routes/users");
let authRoute = require("./routes/auth");

let app = express();
app.use(cors());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);
app.use(bodyParser.json());

mongoose.connect("mongodb://localhost/users", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  });
var db = mongoose.connection;

if(!db) console.log("Error connecting to database !");
else console.log("Database connected");

var port = process.env.SERVICE_PORT || 4000;

app.get("/", (req,res)=>response.success(res, "Welcome to " + process.env.SERVICE_NAME + "-service"));

app.use("/api/v1/",authRoute);
app.use("/api/v1/",usersRoute);

app.use((req,res)=> response.notFound(res, "Endpoint not found!"));

app.listen(port,()=>console.log("Running "+ process.env.SERVICE_NAME +"-service on port " + port));