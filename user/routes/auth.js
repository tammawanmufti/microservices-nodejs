"use-strict";
let router = require('express').Router();
let userController = require('../controller/users')

router
    .route("/auth/login")
    .post(userController.findByCredentials);

module.exports = router;