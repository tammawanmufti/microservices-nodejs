"use-strict";
let router = require('express').Router();
let middleware = require('../../_libs/_middleware');
let usersController = require('../controller/users');

router
    .route('/users')
    .get(usersController.getAllUsers)
    .post(usersController.createUser);

router
    .use(middleware.checkToken)
    .route("/users/:id")
    .get(usersController.getUserById)
    .put(usersController.updateUser)
    .delete(usersController.deleteUser);

module.exports =  router;