const jwt = require('../user/node_modules/jsonwebtoken');
const response = require('./response');

exports.checkToken =  (req,res,next)=>{
    let token = req.headers['authorization'];

    if(token){
        if(token.startsWith('Bearer ')){
            token = token.slice(7,token.length);
        }
        jwt.verify(token, process.env.JWT_KEY,(err,decoded)=>{
            if(err){
                response.unauthorized(res,"Unauthorized");
            } else {
                req.decoded = decoded;
                next();
            }
        })
    } else {
        response.unauthorized(res, "Token not valid");
    }
}