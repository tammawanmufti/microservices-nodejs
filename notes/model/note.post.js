"use strict"
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var NotePost = new Schema({
    title:{
        type:String,
        required:"Note's title is required"
    },
    note:{
        type:String,
        required:"Note is required"
    },
    tag:{
        type:String,
    },
    created_date:{
        type:Date,
        default:Date.now
    },
    created_by:{
        type:String,
        required:"Created by is required"
    },
    modified_at:{
        type:Date,
        default:Date.now
    },
    modified_by:{
        type:String
    }
})

module.exports = mongoose.model("NotesPost",NotePost)