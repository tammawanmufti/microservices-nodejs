"use strict";

const NotePost = require("../model/note.post")
const response = require("../../_libs/response")

exports.getAllPost = function (req, res) {
    let params = {}

    if (req.query.category) {
        params = { category: req.query.category }
    }

    NotePost.find(params)
        .then((data) => {
            response.success(res, "Get all notes data success!", data);
        })
        .catch(err => response.badRequest(res, err.message));
}

exports.createPost = function (req, res) {
    NotePost.create(req.body)
        .then((data) => response.created(res, "Note created!", data))
        .catch(err => response.badRequest(res, err.message))
}

exports.getPostByID = function(req,res){
    NotePost.findOne({_id: req.params.id}) 
        .then((data)=>
            !data
            ?response.notFound(res,"Note not found !")
            :response.success(res,"Note found",data))
        .catch(err => response.badRequest(err, err.message))
}
exports.getPostByTag = function(req,res){
    NotePost.find({tag: req.params.tag}) 
        .then((data)=>
            !data
            ?response.notFound(res,"Note not found !")
            :response.success(res,"Note found",data))
        .catch(err => response.badRequest(err, err.message))
}

exports.getPostByUser = function(req,res){
    NotePost.find({created_by: req.params.user}) 
        .then((data)=>
            !data
            ?response.notFound(res,"Note not found !")
            :response.success(res,"Note found",data))
        .catch(err => response.badRequest(err, err.message))
}

exports.getPostByUserAndTag= function(req,res){
    console.log(req.decoded["data"])
    NotePost.find({created_by: req.query.user,tag: req.query.tag}) 
        .then((data)=>
            !data
            ?response.notFound(res,"Note not found !")
            :response.success(res,"Note found",data))
        .catch(err => response.badRequest(err, err.message))
}

exports.deletePost = function(req,res){
    NotePost.findOneAndDelete({_id: req.params.id})
        .then((data)=>
            !data
            ?response.notFound(res,"Note not found !")
            :response.success(res,"Note deleted!"))
        .catch(err => console.log(err))//response.badRequest(err, err.message))
} 