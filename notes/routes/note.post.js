"use-strict"
let router = require('express').Router()
var notePostController = require('../controller/note.post')
var middleware = require('../../_libs/_middleware')

router
    .use(middleware.checkToken)
    .route("/notes/posts")
    .get(notePostController.getAllPost)
    .post(notePostController.createPost)

router
    .use(middleware.checkToken)
    .route("/notes/posts/:id")
    .get(notePostController.getPostByID)
    .delete(notePostController.deletePost)

router
    .use(middleware.checkToken)
    .route("/notes/tag/:tag")
    .get(notePostController.getPostByTag)
    
router
    .use(middleware.checkToken)
    .route("/notes/user/:user")
    .get(notePostController.getPostByUser)

router
    .use(middleware.checkToken)
    .route("/notes")
    .get(notePostController.getPostByUserAndTag)

module.exports = router