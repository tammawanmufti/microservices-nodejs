require("dotenv").config();
let express = require("express");
let response = require("../_libs/response");
let bodyParser = require("body-parser");
let mongoose = require("mongoose");
let middleware = require("../_libs/_middleware");

let notesPostRoute = require('./routes/note.post')

let app = express()
app.use(
    bodyParser.urlencoded({
        extended:true
    })
)
app.use(bodyParser.json())
app.use(middleware.checkToken)

mongoose.connect(process.env.MONGO_URL,{
    useNewUrlParser : true,
    useUnifiedTopology: true,
    useCreateIndex :true
})

var db=mongoose.connection

!db
?console.log('Error connecting to db!')
:console.log('DB connected !')

var port = process.env.SERVICE_PORT || 4001

app.get('/',(req,res)=> response.success(res, 'Welcome to '+ process.env.SERVICE_NAME +'-service'))

app.use('/api/v1', notesPostRoute)

app.use((req,res) =>response.notFound(res, 'Endpoint not found!'))

app.listen(port,function(){
    console.log('Running '+ process.env.SERVICE_NAME +'-service on Port ' + port)
})

